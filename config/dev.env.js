'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  FIREBASE_CONFIG: {
    apiKey: '"AIzaSyAcKvihKFtbDVVPTuZyMLhZGPxBt1IlaCY"',
    authDomain: '"skin-silmary.firebaseapp.com"',
    databaseURL: '"https://skin-silmary.firebaseio.com"',
    projectId: '"skin-silmary"',
    storageBucket: '"skin-silmary.appspot.com"',
    messagingSenderId: '"680277168870"'
  },
  KakaoAPI: '"https://kapi.kakao.com"',
  KakaoKey: '"cc5e8c2f9815ceb1f6b67008baf6650c"'
})
